package ru.tsc.gulin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.request.ServerAboutRequest;
import ru.tsc.gulin.tm.dto.request.ServerVersionRequest;
import ru.tsc.gulin.tm.dto.response.ServerAboutResponse;
import ru.tsc.gulin.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
