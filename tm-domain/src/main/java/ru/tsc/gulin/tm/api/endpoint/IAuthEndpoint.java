package ru.tsc.gulin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.request.UserLoginRequest;
import ru.tsc.gulin.tm.dto.request.UserLogoutRequest;
import ru.tsc.gulin.tm.dto.request.UserShowProfileRequest;
import ru.tsc.gulin.tm.dto.response.UserLoginResponse;
import ru.tsc.gulin.tm.dto.response.UserLogoutResponse;
import ru.tsc.gulin.tm.dto.response.UserShowProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserShowProfileResponse profile(@NotNull UserShowProfileRequest request);

}