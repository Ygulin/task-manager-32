package ru.tsc.gulin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.gulin.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.gulin.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.gulin.tm.dto.response.TaskUnbindFromProjectResponse;

public interface IProjectTaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

}
