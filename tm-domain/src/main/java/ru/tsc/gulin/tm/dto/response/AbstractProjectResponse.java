package ru.tsc.gulin.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable Project project;

}
