package ru.tsc.gulin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.request.*;
import ru.tsc.gulin.tm.dto.response.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(@NotNull DataJsonFasterXmlLoadRequest request);

    @NotNull
    DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(@NotNull DataJsonFasterXmlSaveRequest request);

    @NotNull
    DataJsonJaxbLoadResponse loadDataJsonJaxb(@NotNull DataJsonJaxbLoadRequest request);

    @NotNull
    DataJsonJaxbSaveResponse saveDataJsonJaxb(@NotNull DataJsonJaxbSaveRequest request);

    @NotNull
    DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(@NotNull DataXmlFasterXmlLoadRequest request);

    @NotNull
    DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(@NotNull DataXmlFasterXmlSaveRequest request);

    @NotNull
    DataXmlJaxbLoadResponse loadDataXmlJaxb(@NotNull DataXmlJaxbLoadRequest request);

    @NotNull
    DataXmlJaxbSaveResponse saveDataXmlJaxb(@NotNull DataXmlJaxbSaveRequest request);

    @NotNull
    DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(@NotNull DataYamlFasterXmlLoadRequest request);

    @NotNull
    DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(@NotNull DataYamlFasterXmlSaveRequest request);

}