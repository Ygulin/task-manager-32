package ru.tsc.gulin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.service.IAuthService;
import ru.tsc.gulin.tm.api.service.IPropertyService;
import ru.tsc.gulin.tm.api.service.IUserService;
import ru.tsc.gulin.tm.exception.field.LoginEmptyException;
import ru.tsc.gulin.tm.exception.field.PasswordEmptyException;
import ru.tsc.gulin.tm.exception.system.AuthenticationException;
import ru.tsc.gulin.tm.model.User;
import ru.tsc.gulin.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User check(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        @Nullable final User user = userService.findOneByLogin(login);
        Optional.ofNullable(user).orElseThrow(AuthenticationException::new);
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        Optional.ofNullable(hash).orElseThrow(AuthenticationException::new);
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return user;
    }

}
