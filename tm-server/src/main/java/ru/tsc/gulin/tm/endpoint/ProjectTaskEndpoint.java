package ru.tsc.gulin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.gulin.tm.api.service.IProjectTaskService;
import ru.tsc.gulin.tm.api.service.IServiceLocator;
import ru.tsc.gulin.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.gulin.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.gulin.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.gulin.tm.dto.response.TaskUnbindFromProjectResponse;

public final class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

}
