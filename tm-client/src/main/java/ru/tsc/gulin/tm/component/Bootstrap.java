package ru.tsc.gulin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.gulin.tm.api.endpoint.*;
import ru.tsc.gulin.tm.api.repository.ICommandRepository;
import ru.tsc.gulin.tm.api.service.*;
import ru.tsc.gulin.tm.client.*;
import ru.tsc.gulin.tm.command.AbstractCommand;
import ru.tsc.gulin.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.gulin.tm.exception.system.CommandNotSupportedException;
import ru.tsc.gulin.tm.repository.CommandRepository;
import ru.tsc.gulin.tm.service.*;
import ru.tsc.gulin.tm.util.SystemUtil;
import ru.tsc.gulin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.gulin.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final AuthEndpointClient authEndpoint = new AuthEndpointClient();

    @Getter
    @NotNull
    private final ProjectEndpointClient projectEndpoint = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final DomainEndpointClient domainEndpoint = new DomainEndpointClient();

    @Getter
    @NotNull
    private final ProjectTaskEndpointClient projectTaskEndpoint = new ProjectTaskEndpointClient();

    @Getter
    @NotNull
    private final SystemEndpointClient systemEndpoint = new SystemEndpointClient();

    @Getter
    @NotNull
    private final TaskEndpointClient taskEndpoint = new TaskEndpointClient();

    @Getter
    @NotNull
    private final UserEndpointClient userEndpoint = new UserEndpointClient(authEndpoint);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void registry(@NotNull final Class clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final Object object = clazz.newInstance();
        final AbstractCommand command = (AbstractCommand) object;
        registry(command);
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutDown));
        fileScanner.start();
    }

    private void prepareShutDown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN**");
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);
        prepareStartup();
        while (true) processCommand();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            processCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (final Exception e) {
            loggerService.error(e);
            System.err.println("[FAIL]");
        }
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
