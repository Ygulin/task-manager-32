package ru.tsc.gulin.tm.command.data;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.Domain;
import ru.tsc.gulin.tm.dto.request.UserLogoutRequest;
import ru.tsc.gulin.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataYamlFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from yaml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD DATA FROM YAML FILE]");
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_YAML)));
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
        @NotNull final Domain domain = yamlMapper.readValue(xml, Domain.class);
        setDomain(domain);
        serviceLocator.getAuthEndpoint().logout(new UserLogoutRequest());
    }

}
