package ru.tsc.gulin.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.command.AbstractCommand;
import ru.tsc.gulin.tm.enumerated.Role;

public class DisconnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        serviceLocator.getAuthEndpoint().disconnect();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
