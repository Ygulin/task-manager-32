package ru.tsc.gulin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.request.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Clear all tasks";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        getTaskEndpoint().clearTask(new TaskClearRequest());
    }

}
