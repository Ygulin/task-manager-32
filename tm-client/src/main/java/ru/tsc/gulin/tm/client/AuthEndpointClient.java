package ru.tsc.gulin.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.gulin.tm.api.endpoint.IEndpointClient;
import ru.tsc.gulin.tm.dto.request.UserLoginRequest;
import ru.tsc.gulin.tm.dto.request.UserLogoutRequest;
import ru.tsc.gulin.tm.dto.request.UserShowProfileRequest;
import ru.tsc.gulin.tm.dto.response.UserLoginResponse;
import ru.tsc.gulin.tm.dto.response.UserLogoutResponse;
import ru.tsc.gulin.tm.dto.response.UserShowProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint, IEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserShowProfileResponse profile(@NotNull final UserShowProfileRequest request) {
        return call(request, UserShowProfileResponse.class);
    }

}
